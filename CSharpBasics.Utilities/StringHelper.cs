﻿using System;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, сим2волы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
			throw new NotImplementedException();
		}
	}
}